#encoding=utf-8

from operator import *

class Graph(object):

	def __init__(self, s):
		self.seq = s
		self.level = s[-1]  #迷宫层数
		self.lastpoint_l = None  #左边最后一对数
		self.lastpoint_r = None  #右边最后一对数
		self.extra_flag = 0  #表示左边是否加了额外竖直辅助线，加了为1，不加为0

		self.base_line_r = [0]  #表示目前主干竖线
		self.base_line_l = [0]  #表示目前主干竖线
		self.base_line = 0   # 同一条主干竖线被用多次会pop空，用此值记录前一次的baseline留用

		self.even_list = []  #偶数开头的数对，骨架左边
		self.odd_list = [] #奇数开头的数对，骨架右边

		self.list_init()

		self.coor = [] #所有点的坐标
		self.coor_r = [] #右边所有点的坐标
		self.lines_v = [] #所有连接上的垂直线段
		self.lines_h = [] #所有连接上的水平线段

		# self.flag_endpoint = {} # 1表示点为已连接点，0表示点为待连接点
		self.point_l = []
		self.point_r = []
		self.point_e = []

		self.flag_l = 0 #现在操作所在左边缘，left flag
		self.flag_b = 0 #现在操作所在下边缘，bottom flag
		self.skeleton_left(self.even_list)

		#左边用完之后初始化给右边用
		self.flag_r = 1 #现在操作所在左边缘，right flag
		self.flag_b = 0 #现在操作所在下边缘，bottom flag
		self.skeleton_right(self.odd_list)

		self.point_l = sorted(self.point_l, key=itemgetter(1), reverse=True) 
		self.point_r = sorted(self.point_r, key=itemgetter(1), reverse=True)
		self.point_e = sorted(self.point_e, key=itemgetter(1), reverse=True)

		if self.level%2:
			self.point_r.insert(0, self.point_e[0])
			self.right = self.point_r[0][0]
			self.left = self.point_l[0][0]
			self.bottom = self.point_r[0][1] +1
		else:
			self.point_l.insert(0, self.point_e[0])
			self.right = self.point_e[0][0] + 1
			self.left = self.point_e[0][0]
			self.bottom = self.point_e[0][1]

		self.point_r.append(self.point_e[1])

		

		# print("SIGNAL",self.left, self.right, self.bottom)



	def list_init(self):
		even_temp = []
		odd_temp = []
		for i in range(self.level):
			temp = [self.seq[i],self.seq[i+1]]
			temp.sort()
			temp.append(abs(self.seq[i]-self.seq[i+1]))
			if i%2==0:
				even_temp.append(temp)  
			else:
				odd_temp.append(temp)


		even_temp = sorted(even_temp, key=itemgetter(0, 1))#, reverse=True) 
		odd_temp = sorted(odd_temp, key=itemgetter(0, 1))
		# print("even_temp",even_temp,"odd_temp",odd_temp)
		
		#找到左右两边最后一个点
		self.lastpoint_r = odd_temp[-1][:2]
		self.lastpoint_l = even_temp[-1][:2]
 
		for item in even_temp:
			self.iterator(item[:2], self.even_list)
		for item in odd_temp:
			self.iterator(item[:2], self.odd_list)
		print ("even_list", self.even_list, "odd_list",self.odd_list)

	#递归求区间的包含关系
	def iterator(self, obj, lst):
		temp = lst[:]
		for item in temp:
			#如果obj包含在当前空间内，继续寻找，看是否被包含于更深层的列表中
			if obj[0]>item[0] and obj[1]<item[1]:
				#如果item[2:]不为空，说明还有更深层的列表，继续递归。否则直接添加item
				if item[2:]:
					xxx = self.iterator(obj, item[2:])
					item[2:] = xxx
					break
				else:
					# item.insert(2, obj)
					item.append(obj)
					break
			# elif item == temp[-1]:  #如果遍历完所有的子列表，都没找到item被包含的，则item被添加在主列表中
			# 	lst.insert(0, obj)
		else:
			# lst.insert(0, obj)
			lst.append(obj)
		return lst

	def skeleton_left(self, lst):		
		# print("lst", lst)
		# print("flag",self.flag_l,self.flag_b)
		# print("lines_v",self.lines_v,"lines_h",self.lines_h)
		for item in lst:
			# print("item", item)
			# if type(item[0]) == int:
			# 	if len(item[2:]) > 1:
			# 		self.base_line_l.append(self.flag_l)
			# print(self.base_line_l)
			gap = item[1] - item[0]
			# print("gap", gap)
			if len(lst) == 1 and gap >1: 
				#说明左半部分为一个整体，没有引入额外辅助竖线,且第一对数字，包含所有
				#画出左边的主干竖线
				self.coor.append((self.flag_l, self.flag_b))
				self.coor.append((self.flag_l, self.flag_b+gap-1))

				#更新待操作坐标位置
				self.flag_b += 1
				self.flag_l -= 1

				# #标记这条线的两个端点都是待连接点
				# self.flag_endpoint[self.coor[-2]] = 0
				# self.flag_endpoint[self.coor[-1]] = 0
				self.point_l.append(self.coor[-2])
				self.point_l.append(self.coor[-1])

				# self.lines_v.append(str(self.coor[-2][:2])+"--"+str(self.coor[-1][:2])) #将当前垂线加入lines_v
				self.lines_v.append([self.coor[-2][:2],self.coor[-1][:2]])

				self.skeleton_left(item[2:])

			elif gap > 1:
				# print ("gap > 1")
				#说明左半部分不是一个整体，要引入额外辅助线，长度为 gap+1
				if self.flag_l == self.flag_b and self.flag_b == 0:
					# print("extra")
					self.extra_flag = 1  #加了额外竖直辅助线，更新flag
					length = gap + 1
					#画出左边的主干竖线
					self.coor.append((self.flag_l, self.flag_b))
					self.coor.append((self.flag_l, self.flag_b+self.level-1))

					# self.base_line_l.append(self.flag_l)  #记录本次主干竖线位置

					#更新待操作坐标位置,左边不需要下移一位
					# self.flag_b += 1
					self.flag_l -= 1

					# #标记这条线的两个端点都是待连接点
					# self.flag_endpoint[self.coor[-2]] = 0
					# self.flag_endpoint[self.coor[-1]] = 0
					self.point_e.append(self.coor[-2])    ######## modified in 14:04
					self.point_e.append(self.coor[-1])
					# print("ffffffffffff", self.point_e)
					# self.lines_v.append(str(self.coor[-2][:2])+"--"+str(self.coor[-1][:2])) #将当前垂线加入lines_v
					self.lines_v.append([self.coor[-2][:2], self.coor[-1][:2]])
				# else:
				# 	base_l = self.flag_l


				#画出这个item代表的主线
				# print("ddddddd",self.flag_l)
				self.coor.append((self.flag_l, self.flag_b))
				self.coor.append((self.flag_l, self.flag_b+gap-1))
				
				# self.base_line_l.append(self.flag_l)  #记录本次主干竖线位置

				#更新待操作坐标位置
				self.flag_b += 1
				self.flag_l -= 1

				#标记这条线的两个端点都是待连接点
				# self.flag_endpoint[self.coor[-2]] = 0
				# self.flag_endpoint[self.coor[-1]] = 0
				self.point_l.append(self.coor[-2])
				self.point_l.append(self.coor[-1])

				# self.lines_v.append(str(self.coor[-2][:2])+"--"+str(self.coor[-1][:2])) #将当前垂线加入lines_v
				self.lines_v.append([self.coor[-2][:2], self.coor[-1][:2]])

				self.skeleton_left(item[2:])

				#如果本item不是lst的最后一个item，需要加分隔线
				if item != lst[-1]:
					# print(item, lst,"ssssssss")

					# #更新待操作坐标位置
					# self.flag_b += 1
					# print("########",self.base_line_l)
					# left = self.base_line
					# if self.base_line_l:
						# left = self.base_line_l.pop()
						# self.base_line = left
					#点下画一个分割线
					# self.coor.append((left, self.flag_b))
					# self.coor.append((left-1, self.flag_b))

					self.coor.append((self.flag_l+1, self.flag_b))
					self.coor.append((self.flag_l+2, self.flag_b))

					#分割线的左端点是待连接点，右端点是已连接点
					# self.flag_endpoint[self.coor[-2]] = 0
					# self.flag_endpoint[self.coor[-1]] = 1
					self.point_l.append(self.coor[-2])
					if self.coor[-1] in self.point_l:
						self.point_l.remove(self.coor[-1])

					# self.lines_h.append(str(self.coor[-2][:2])+"--"+str(self.coor[-1][:2]))
					self.lines_h.append([self.coor[-1][:2], self.coor[-2][:2]])
					# print("lines extra", self.lines_h[-1])
					#更新待操作坐标位置？？？？？？？？？？？？？？
					self.flag_b += 1
					self.flag_l += 1

				
			elif gap == 1:				
				if self.flag_l == self.flag_b and self.flag_b == 0:
					# print("extra")
					self.extra_flag = 1  #加了额外竖直辅助线，更新flag
					length = gap + 1
					#画出左边的主干竖线
					self.coor.append((self.flag_l, self.flag_b))
					self.coor.append((self.flag_l, self.flag_b+self.level-1))

					# self.base_line_l.append(self.flag_l)  #记录本次主干竖线位置

					#更新待操作坐标位置,左边不需要下移一位
					# self.flag_b += 1
					self.flag_l -= 1

					# #标记这条线的两个端点都是待连接点
					# self.flag_endpoint[self.coor[-2]] = 0
					# self.flag_endpoint[self.coor[-1]] = 0
					self.point_e.append(self.coor[-2])    ######## modified in 14:04
					self.point_e.append(self.coor[-1])
					# print("ffffffffffff", self.point_e)
					# self.lines_v.append(str(self.coor[-2][:2])+"--"+str(self.coor[-1][:2])) #将当前垂线加入lines_v
					self.lines_v.append([self.coor[-2][:2], self.coor[-1][:2]])

				#为一点，把点加入coor
				self.coor.append((self.flag_l, self.flag_b))

				#标记该点为待连接点
				# self.flag_endpoint[self.coor[-1]] = 0
				self.point_l.append(self.coor[-1])

				# print("self.lastpoint_l:",self.lastpoint_l)
				if item != self.lastpoint_l:
					#当前这个点，不是最后一个点，加分割横线

					#更新待操作坐标位置
					self.flag_b += 1

					#点下画一个分割线
					self.coor.append((self.flag_l+1, self.flag_b))
					self.coor.append((self.flag_l, self.flag_b))    
					# print("sssssss",self.flag_b,self.flag_l)
					#分割线的左端点是待连接点，右端点是已连接点
					# self.flag_endpoint[self.coor[-2]] = 0
					# self.flag_endpoint[self.coor[-1]] = 1
					self.point_l.append(self.coor[-1])
					if self.coor[-2] in self.point_l:
						self.point_l.remove(self.coor[-2])


					# self.lines_h.append(str(self.coor[-2][:2])+"--"+str(self.coor[-1][:2]))
					self.lines_h.append([self.coor[-1][:2], self.coor[-2][:2]])
					# print("lines extra2",self.lines_h[-1])
					#更新待操作坐标位置？？？？？？？？？？？？？？
					self.flag_b += 1

				#是最后一个点，则左半部分结束，不需要更新坐标了

	def skeleton_right(self, lst):
		for item in lst:
			gap = item[1] - item[0]
			# print("gap",gap,"item",item)
			# if type(item[0]) == int:
			# 	if len(item[2:]) > 1:
			# 		self.base_line_r.append(self.flag_r)
			# # print(self.base_line_r)
			if len(lst) == 1 and gap > 1:
			#说明右半部分为一个整体
				#extra_flag == 0表示左边未曾引入过额外辅助竖线，此刻要加
				if self.flag_r == 1 and self.flag_b == 0 and self.extra_flag == 0:
				#说明右半部分不是一个整体，如果左边没有引入额外辅助线，则要引入额外辅助线，长度为 gap+1
					# print("extra")
					self.extra_line()

				#画出左边的主干竖线
				self.coor_r.append((self.flag_r, self.flag_b))
				self.coor_r.append((self.flag_r, self.flag_b+gap-1))

				#更新待操作坐标位置
				self.flag_b += 1
				self.flag_r += 1

				#标记这条线的两个端点都是待连接点
				# self.flag_endpoint[self.coor_r[-2]] = 0
				# self.flag_endpoint[self.coor_r[-1]] = 0
				self.point_r.append(self.coor_r[-2])
				self.point_r.append(self.coor_r[-1])

				# self.lines_v.append(str(self.coor_r[-2][:2])+"--"+str(self.coor_r[-1][:2])) #将当前垂线加入lines_v
				self.lines_v.append([self.coor_r[-2][:2],self.coor_r[-1][:2]])
	
				self.skeleton_right(item[2:])
			elif gap > 1:
				# print ("gap > 1", lst, item)
				if self.flag_r == 1 and self.flag_b == 0 and self.extra_flag == 0:
				#说明右半部分不是一个整体，如果左边没有引入额外辅助线，则要引入额外辅助线，长度为 gap+1
					self.extra_line()
				elif self.flag_r == 1 and self.flag_b == 0:  ####### added in 13:55
				# 初始时，左边加过额外辅助线，且右边也不是一个整体，则右边下移一格继续操作
					self.flag_b -= 1

				#画出这个item代表的主线
				self.coor_r.append((self.flag_r, self.flag_b))
				self.coor_r.append((self.flag_r, self.flag_b+gap-1))
				
				#更新待操作坐标位置
				self.flag_b += 1
				self.flag_r += 1

				#标记这条线的两个端点都是待连接点
				# self.flag_endpoint[self.coor_r[-2]] = 0
				# self.flag_endpoint[self.coor_r[-1]] = 0
				self.point_r.append(self.coor_r[-2])
				self.point_r.append(self.coor_r[-1])

				# self.lines_v.append(str(self.coor_r[-2][:2])+"--"+str(self.coor_r[-1][:2])) #将当前垂线加入lines_v
				self.lines_v.append([self.coor_r[-2][:2],self.coor_r[-1][:2]])
				# print("lines", self.lines_v[-1])
				self.skeleton_right(item[2:])

				#如果本item不是lst的最后一个item，需要加分隔线
				if item != lst[-1]:
					# print(item, lst,"ssssssss")

					# #更新待操作坐标位置
					# self.flag_b += 1
					# right = self.base_line
					# if self.base_line_r:
					# 	right = self.base_line_r.pop()
					# 	self.base_line = right

					self.coor_r.append((self.flag_r-1, self.flag_b))
					self.coor_r.append((self.flag_r-2, self.flag_b))

					#分割线的右端点是待连接点，左端点是已连接点
					# self.flag_endpoint[self.coor_r[-2]] = 1
					# self.flag_endpoint[self.coor_r[-1]] = 0
					self.point_r.append(self.coor_r[-2])
					if self.coor_r[-1] in self.point_r:
						self.point_r.remove(self.coor_r[-1])

					# self.lines_h.append(str(self.coor_r[-2][:2])+"--"+str(self.coor_r[-1][:2]))
					self.lines_h.append([self.coor_r[-2][:2], self.coor_r[-1][:2]])
					# print("extra1", self.lines_h[-1])
					#更新待操作坐标位置？？？？？？？？？？？？？？
					self.flag_b += 1
					self.flag_r -= 1

			elif gap == 1:				
				if self.flag_r == 1 and self.flag_b == 0 and self.extra_flag == 0:
					#说明右半部分不是一个整体，如果左边没有引入额外辅助线，则要引入额外辅助线，长度为 gap+1
					self.extra_line()				
				elif self.flag_r == 1 and self.flag_b == 0:  ####### added in 13:55
					# 初始时，左边加过额外辅助线，且右边也不是一个整体，则右边下移一格继续操作
					self.flag_b += 1

				#为一点，把点加入coor
				self.coor_r.append((self.flag_r, self.flag_b))
				#标记该点为待连接点
				# self.flag_endpoint[self.coor_r[-1]] = 0
				self.point_r.append(self.coor_r[-1])
				# print("point", self.point_r[-1])

				# print("self.lastpoint_r:",self.lastpoint_r)
				if item != self.lastpoint_r:
					#当前这个点，不是最后一个点，加分割横线

					#更新待操作坐标位置
					self.flag_b += 1

					#点下画一个分割线
					self.coor_r.append((self.flag_r-1, self.flag_b))
					self.coor_r.append((self.flag_r, self.flag_b))

					#分割线的左端点是待连接点，右端点是已连接点
					# self.flag_endpoint[self.coor_r[-2]] = 1
					# self.flag_endpoint[self.coor_r[-1]] = 0
					self.point_r.append(self.coor_r[-1])
					if self.coor_r[-2] in self.point_r:
						self.point_r.remove(self.coor_r[-2])

					# self.lines_h.append(str(self.coor_r[-2][:2])+"--"+str(self.coor_r[-1][:2]))
					self.lines_h.append([self.coor_r[-2][:2], self.coor_r[-1][:2]])
					# print("extra2", self.lines_h[-1])

					#更新待操作坐标位置？？？？？？？？？？？？？？
					self.flag_b += 1

				#是最后一个点，则左半部分结束，不需要更新坐标了

		#奇数层maze，底端多加横线
		if self.level%2:
			self.lines_h.append([self.point_e[-1], (self.point_e[-1][0]+1,self.point_e[-1][1])])
			# print("sgggggggg", self.point_e)
			self.point_e[-1] = (self.point_e[-1][0]+1,self.point_e[-1][1])


	def extra_line(self):
		# print("extra")
		self.extra_flag = 1
		#画出右边的主干竖线
		self.coor_r.append((1, 0))
		self.coor_r.append((1, self.level-1))

		#更新待操作坐标位置,右边需要下移一位
		self.flag_b += 1
		self.flag_r += 1

		#标记这条线的两个端点都是待连接点
		# self.flag_endpoint[self.coor_r[-2]] = 0
		# self.flag_endpoint[self.coor_r[-1]] = 0

		self.point_e.append(self.coor_r[-1])
		self.point_e.append(self.coor_r[-2])

		# self.lines_v.append(str(self.coor_r[-2][:2])+"--"+str(self.coor_r[-1][:2])) #将当前垂线加入lines_v
		self.lines_v.append([self.coor_r[-2][:2], self.coor_r[-1][:2]])

	def bind(self):
		temp_v = []
		temp_h = []
		for i in range(len(self.point_l)):
			now_l = self.point_l[i]
			now_r = self.point_r[i]
			# print("now", now_l, now_r, self.left, self.right, self.bottom)
			if now_l[0] == self.left and now_l[1] == self.bottom and now_r[0] == self.right and now_r[1] == self.bottom and self.level%2 == 0:
				temp_h.append([now_l, now_r])			
			elif now_l[0] == self.left and now_r[0] == self.right:
				temp_v.append([now_l,(self.left,self.bottom)])
				temp_v.append([now_r, (self.right,self.bottom)])
				temp_h.append([(self.left,self.bottom),(self.right,self.bottom)])
				# print("temp_v1",temp_v[-2:])
			elif now_r[0] == self.right:
				temp_v.append([(self.left, now_l[1]),(self.left,self.bottom)])
				temp_v.append([now_r, (self.right,self.bottom)])
				temp_h.append([(self.left, now_l[1]),now_l])				
				temp_h.append([(self.left,self.bottom),(self.right,self.bottom)])
				# print("temp_v2",temp_v[-2:])
			elif now_l[0] == self.left:
				temp_v.append([now_l,(self.left,self.bottom)])
				temp_v.append([(self.right,now_r[1]),(self.right,self.bottom)])
				temp_h.append([(self.right, now_r[1]),now_r])				
				temp_h.append([(self.left,self.bottom),(self.right,self.bottom)])
				# print("temp_v3",temp_v[-2:])
			else:
				temp_v.append([(self.left,now_l[1]),(self.left,self.bottom)])
				temp_v.append([(self.right,now_r[1]), (self.right,self.bottom)])
				temp_h.append([(self.right, now_r[1]), now_r])
				temp_h.append([(self.left, now_l[1]),now_l])						
				temp_h.append([(self.left,self.bottom),(self.right,self.bottom)])
				# print("temp_v4",temp_v[-2:])
			self.left -= 1
			self.right += 1
			self.bottom += 1


		# print("temp_v",temp_v)
		# print("temp_h",temp_h)

		# print("lines_v",self,lines_v)
		# print("lines_h",self.lines_h)

		self.merage(self.lines_v, temp_v)
		self.merage(self.lines_h, temp_h)

	def merage(self, lines1, lines2):
		lines = lines1[:]
		for item in lines2: 	
			for k in lines: 
				if item[0] in k:
					# print("item",item,"k",k)
					lines1 = self.ele_operate(item, k, lines1)
					break
				elif item[1] in k: 
					# print("item",item,"k",k)
					lines1 = self.ele_operate(item, k, lines1)
					break
				elif k==lines[-1]:
					if item not in lines1:
						lines1.append(item)
			else:
				if item not in lines1:
					lines1.append(item)
		return lines1

	def ele_operate(self, item1, item2, lines):	
		if item1[0] in item2:	
			i=item1[0]
		elif item1[1] in item2:
			i = item1[1]		
		temp_item1 = item1[:]
		temp_item2 = item2[:]
		temp_item2.remove(i)
		temp_item1.remove(i)
		lines.remove(item2)
		lines.append([temp_item2[0], temp_item1[0]])
		 
		return lines

	def lines(self):
		self.bind()
		# print("point_l:",self.point_l)
		# print("point_r:",self.point_r)
		# print("point_e:",self.point_e)
		# print("lines_h:", self.lines_h)
		# print("lines_v:", self.lines_v)
		self.lines_v.sort()
		self.lines_h.sort()
		# print("sssssss",self.lines_v)
		max_left = self.lines_v[0][0][0]
		# print(max_left)

		lines_v = []
		for item in self.lines_v:
			lines_v.append([])
			for i in item:
				i = list(i)
				i[0] += abs(max_left)
				lines_v[-1].append(tuple(i))

		lines_h = []
		for item in self.lines_h:
			lines_h.append([])
			for i in item:
				i = list(i)
				i[0] += abs(max_left)
				lines_h[-1].append(tuple(i))

		print("lines_h:", lines_h)
		print("lines_v:", lines_v)
		return lines_h, lines_v


	
if __name__ == '__main__':	
	s = [0, 1, 2, 3]
	s = [0,3,2,1,4]
	# s = [0, 7, 6, 5, 4, 3, 2, 1, 8]
	# s = [0, 5, 4, 1, 2, 3, 6]
	# s = [0, 7, 2, 3, 6, 5, 4, 1, 8 ]
	# s = [0, 9, 8, 5, 6, 7, 4, 1, 2, 3, 10]
	# s = [0, 7, 6, 5, 4, 1, 2, 3, 8, 11, 10, 9, 12, 13, 14]
	# s = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 13, 12, 11, 10, 15]

	lines_h, lines_v = Graph(s).lines()
	print(lines_h)
	print(lines_v)