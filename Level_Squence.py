#encoding=utf-8
from graph import Graph

class Maze(object):

	def __init__(self, squence):
		self.list = squence.split(" ")
		# print(self.list)
		self.length = len(self.list)
		# print(self.length)


	#判断序列是否奇偶间隔
	def odd_even_alt(self):
		for i in range(0, self.length, 2):
			if i%2 == 0:
				return True
			else:
				return False

	#判断序列是否全由数字组成
	def all_numbers(self):
		try:
			for i in range(len(self.list)):
				self.list[i] = int(self.list[i])
			return True
		except:
			return False

	#判断序列是否从0开始以最大值结束
	def start_end(self):
		temp = self.list[:]
		temp.sort()
		if temp[-1] == self.list[-1] and self.list[0] == 0:
			return True
		else:
			return False

	#判断重叠区间是否相互包含
	def overlap(self):
		group = []
		for i in range(1,len(self.list),2):
			group.append([self.list[i-1], self.list[i]])
			group[-1].sort()

		group.sort()
		# print("group",group)
		for i in group:
			for j in group:
				if (i[0]>j[0] and i[1]>j[1] and i[1]<j[0]) or (i[0]<j[0] and i[1]<j[1] and i[0]>j[1]):
					return False

		return True


	#判断序列数字是否全部连续
	def continuity(self):
		temp = self.list[:]
		temp.sort()
		for i in range(self.length-1):
			if temp[i+1] - temp[i] > 1:
				return False
			else:
				return True

	def latex_produce(self, s):
		myfile = open(s+".tex", "w+")
		tempfile = open("state.tex", "r")

		myfile.writelines(tempfile.read())

		lines_h, lines_v = Graph(self.list).lines()
		myfile.write("% Horizontal lines\n")
		for i in lines_h:
			i.sort()
			myfile.write("\draw"+str(i[0])+"--"+str(i[1])+";\n")

		myfile.write("% Vertical lines\n")
		for i in lines_v:
			i.sort()
			myfile.write("\draw"+str(i[0])+"--"+str(i[1])+";\n")

		myfile.write("\end{tikzpicture}\n\end{center}\n\\vspace*{\\fill}\n\n\end{document}")

	def generate_latex_code(self, name):
		if self.length>3 and self.length<21:
			# print(1)
			if self.all_numbers():
				# print(2)
				if self.start_end():
					# print(3)
					if self.continuity():
						# print(4)
						if self.odd_even_alt():
							# print(5)
							if self.overlap():
								self.latex_produce(name)
							else:
								return "The input defines overlapping pairs."
						else:
							return "The input should alternate between even and odd numbers."
					else:
						return "The input should consist of 0, 1, 2... N, in some order."
				else:
					return "The input should start with 0 and end with the largest number."
			else:
				return "The input should consist of numbers."
		else:
			return "The input should consist of between 4 and 20 numbers."


if __name__ == '__main__':	
	# s = '0 1 2 3'
	s = '0 3 2 1 4'
	# s ='0 5 4 1 2 3 6'
	# s = '0 7 6 5 4 3 2 1 8'
	name = "my_maze_1"
	num = 0
	maze = Maze(s)
	maze.generate_latex_code(name)